package com.example.home_work_3.model;

import org.springframework.stereotype.Component;

import java.util.ResourceBundle;
import java.util.Scanner;

@Component("locale")
public class Locale {
    ResourceBundle resourceBundle;

    public Locale() {
        while (resourceBundle == null) {
            Scanner scanner = new Scanner(System.in);
            System.out.println("Choose locale: EN, IT, FR");
            String value = scanner.nextLine();
            switch (value) {
                case "EN":
                    resourceBundle = ResourceBundle.getBundle("message");
                    break;
                case "IT":
                    resourceBundle = ResourceBundle.getBundle("message", new java.util.Locale("it"));
                    break;
                case "FR":
                    resourceBundle = ResourceBundle.getBundle("message", new java.util.Locale("fr"));
                    break;
                default:
                    System.out.println("Locale not found. Try again!");
                    break;
            }
        }
    }
}
