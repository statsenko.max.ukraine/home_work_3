package com.example.home_work_3.model;

import com.example.home_work_3.ReaderCSV;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

@Component("personalKYC")
public class PersonalKYC {
    private final Person person;
    private final Map<String, String> questionsAndAnswers = new HashMap<>();

    public PersonalKYC(Person person) {
        this.person = person;

        List<String> questions = ReaderCSV.readCSVAllDataAtOnce(person.getLocale().resourceBundle.getString("csv.file.path"));

        questions.forEach(question -> {
            System.out.println(question);
            Scanner scanner = new Scanner(System.in);
            System.out.println(person.getLocale().resourceBundle.getString("answer"));
            questionsAndAnswers.put(question, scanner.nextLine());
        });
    }

    @Override
    public String toString() {
        return person + "Questions and answers = " + questionsAndAnswers;
    }
}
