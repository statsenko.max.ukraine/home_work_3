package com.example.home_work_3.model;

import lombok.Getter;
import org.springframework.stereotype.Component;

import java.util.Scanner;

@Getter
@Component("person")
public class Person {
    private final Locale locale;
    private final String firstName;
    private final String lastName;

    public Person(Locale locale) {
        this.locale = locale;

        Scanner scanner = new Scanner(System.in);
        System.out.println(locale.resourceBundle.getString("first.name"));
        this.firstName = scanner.nextLine();
        System.out.println(locale.resourceBundle.getString("last.name"));
        this.lastName = scanner.nextLine();
    }

    public String toString() {
        return locale.resourceBundle.getString("first.name") + " = " + this.firstName + ", " + locale.resourceBundle.getString("last.name") + " = " + this.lastName + "\n";
    }
}
