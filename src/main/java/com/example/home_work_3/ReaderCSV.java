package com.example.home_work_3;

import com.opencsv.CSVReader;

import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class ReaderCSV {
    public static List<String> readCSVAllDataAtOnce(String file) {
        List<String[]> allData = new ArrayList<>();
        try {
            FileReader filereader = new FileReader(file);
            CSVReader csvReader = new CSVReader(filereader);
            allData = csvReader.readAll();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return allData
                .stream()
                .map(v -> String.join(",", v))
                .collect(Collectors.toList());
    }
}
