package com.example.home_work_3;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("com.example.home_work_3")
public class SpringConfig {
}
